## HDF 데이터 처리 자동화 및 시각화 도구

### objective
1. 고속 처리 자동화: HDF 파일에서 dry mass/volume 정보를 신속하게 처리하기 위함
2. 시각화 도구: 간단한 시각화 도구를 통해 삼차원 데이터를 신속하게 확인

## prerequisite
1. HDF file handler: Tomocell
2. data handler: numpy, pandas
3. visualization: plotly


### tutorial

wiki page 참고 (링크 추가 예정)
